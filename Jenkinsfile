//global
def app
pipeline {
    agent {
        node {label 'maven'}
    }
    environment {
        //DYNAMIC
        APPLICATION_NAME = 'prasad-app'    // Project and app name
        GIT_BASE = "https://prasadsajjatngov@bitbucket.org/prasadsajjatngov"
        NEXUS_URL = "nexus-nexus3.7163.dhs-tn.openshiftapps.com"
        JENKINS_CRED = "4141f955-9fb2-4891-8e44-c25297ba3aaa"

        //STATIC
        BASE_IMAGE = "s2i-java"
        DEV_PROJECT = "${APPLICATION_NAME}-dev" // Dev name space
        QA_PROJECT = "${APPLICATION_NAME}-qa"   // QA name space
        UAT_PROJECT = "${APPLICATION_NAME}-uat" // UAT name space
        PROD_PROJECT = "${APPLICATION_NAME}"    // PROD name space
        GIT_REPO = "${GIT_BASE}/${APPLICATION_NAME}.git"
        GIT_CI_CD_REPO = "${GIT_BASE}/ci-cd.git"
        MVN_COMMAND_CLEAN_PACKAGE = "mvn clean package" //Maven command to clean the package before build
        MVN_COMMAND_VERIFY = "mvn clean install -DskipTests" //Maven command to build by skipping unit tests
        MVN_COMMAND_PACKAGE ="mvn package -DskipTests=true" //Maven command to package
        MVN_TOOL = "Maven 3.6.0" // Maven version in Jenkins. This should match with the tool name configured in your jenkins instance (JENKINS_URL/configureTools/)
        NEXUS_VERSION = "nexus3"  // Nexus version installed
        NEXUS_PROTOCOL = "http" // This can be http or https to connect to NEXUS
        NEXUS_REPOSITORY =  "${APPLICATION_NAME}" // Repository where we will upload the artifact
        NEXUS_CREDENTIAL_ID = "nexus-credentials"  // Jenkins credential id to authenticate to Nexus openShift.
    }
    stages {

        //stage to check out the code from repo
        stage('Checkout code') {
            steps {
                //checkout master code
                checkout([$class: 'GitSCM',
                          branches: [[name: "master"]],
                          doGenerateSubmoduleConfigurations: false,
                          extensions:[[$class: 'CloneOption', noTags: true, reference: '', shallow: true]],
                          userRemoteConfigs: [[credentialsId: JENKINS_CRED, url: GIT_REPO]]])
            }
        } //stage checkout end
        //stage to build the code for extra check
        stage('Build code') {
            steps {
                // MVN build
                sh "${MVN_COMMAND_VERIFY}"
            }
        } //stage build code end
//        stage('SonarQube') { //stage to SonarQube execution
//            steps{
//                //steps to perform
//            }
//            // code for SonarQube
//        } //stage sonarQube end
        //stage for creating the image. This stage will only run for first time.
        stage('Create Image Builder') {
            //TODO: Check if project exists or create it
            when {
                expression {
                    openshift.withCluster() {
                        openshift.withProject(DEV_PROJECT) {
                            //if the build config exist - Go to next stage.
                            return !openshift.selector("bc", "${APPLICATION_NAME}").exists();
                        }
                    }
                } //expression end
            } //when condition end
            steps {
                script {
                    echo 'Image builder step is being executed'
                    openshift.withCluster() {
                        openshift.withProject(DEV_PROJECT) {
                            // TODO: oc new-app registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift~<git_repo_URL>
                            app = openshift.newApp('registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift~${GIT_BASE}/${APPLICATION_NAME}')
                            def bc = app.narrow('bc')
                            //Verify the deployment - extend the sleep timer for 20 milliseconds while status becomes available for verification
                                bc.untilEach(1) { // We want a minimum of 1 build
                                    // Unlike watch(), untilEach binds 'it' to a Selector for a single object.
                                    // Thus, untilEach will only terminate when all selected objects satisfy this
                                    // the condition established in the closure body (or until the timeout(10)
                                    // interrupts the operation).
                                    return it.object().status.phase == "Complete"
                                } // build untill end
                        } //with project end
                    } // Create the new build and continue
                } //script end
            } // step create image builder ends
        } //stage create image builder ends
        //Stage to start the build
        stage('Start build') {
            steps {
                echo 'Starting the build'
                script {
                    openshift.withCluster() {
                        openshift.withProject(env.DEV_PROJECT) {
                            def bc = openshift.selector('bc', env.APPLICATION_NAME)
                            bc.startBuild()
                            def builds = bc.related("builds")
                            //Wait up to 10 mnts for the build to complete.
                            timeout(10) {
                                builds.untilEach(1) {
                                    return (it.object().status.phase == "Complete")
                                }
                            }
                        }
                    }
                } //script execution end
            }
        } //stage start build ends
        //This stage will execute only for the first time and for the new image only
        stage('Verify the deployment with DC files') {
/*            when {
                expression {
                    openshift.withCluster() {
                        openshift.withProject(env.DEV_PROJECT) {
                            //if the deployment config exist - Go to next stage. No need to deploy as the new build will deploy the latest.
                            return !openshift.selector('dc', "${APPLICATION_NAME}").exists()
                        }
                    }
                }
            } //when condition end*/
            steps {
                echo 'Starting the deployment'
                script {
                    openshift.withCluster() {
                        openshift.withProject(env.DEV_PROJECT) {
                            // def app = openshift.newApp("${APPLICATION_NAME}:latest")
                            //TODO: Use 3Scale instead of expose
                            //app.narrow("svc").expose("--port=8080");
                            def dc = openshift.selector("dc", "${APPLICATION_NAME}")
                            //Verify the deployment - extend the sleep timer for 20 milliseconds while status becomes available for verification
                            while (dc.object().spec.replicas != dc.object().status.availableReplicas) {
                                sleep 500
                            }
                        }
                    }
                }
            }
        } //stage deploy to dev ends
        stage('Run tests') {
            // run the test scripts to verify the deployment
            steps {
                echo 'Starting the dev deployment testing'
                script {
                    try {
                        //TODO: Call service through 3Scale (${DEV_PROJECT}.tn.gov/health)
                        sh "curl -s --head --request GET http://${APPLICATION_NAME}-${DEV_PROJECT}.7163.dhs-tn.openshiftapps.com | grep '200 OK'"
                        return true
                    } catch (Exception e) {
                        echo 'dev deployment testing failed'
                        return false
                    }
                }
            }
        }
        stage('package the application to upload') {
                    steps {
                        script {
                            // package it to store
                            sh "${MVN_COMMAND_PACKAGE}"
                        }
                    } //steps end
                } //Stage end
        // stage to store the artifacts in nexus
        stage('Tag and Upload to NEXUS artifacts') {
            steps {
                script {
                    // Read POM XML file to get the package information like version and packaging
                    pom = readMavenPom file: "pom.xml";
                    // Find built artifact under target folder
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
                    // print some info from the artifact found
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    // read the path from the File found
                    artifactPath = filesByGlob[0].path;
                    // verify artifact name exists
                    artifactExists = fileExists artifactPath;
                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";
                        nexusArtifactUploader(
                                nexusVersion: NEXUS_VERSION,
                                protocol: NEXUS_PROTOCOL,
                                nexusUrl: NEXUS_URL,
                                groupId: pom.groupId,
                                version: pom.version,
                                repository: NEXUS_REPOSITORY,
                                credentialsId: NEXUS_CREDENTIAL_ID,
                                artifacts: [
                                        // Artifact generated such as .jar, .ear and .war files.
                                        [artifactId: pom.artifactId,
                                         classifier: '',
                                         file: artifactPath,
                                         type: pom.packaging],
                                        // upload the pom.xml file for additional information for Transitive dependencies
                                        [artifactId: pom.artifactId,
                                         classifier: '',
                                         file: "pom.xml",
                                         type: "pom"]
                                ]
                        );
                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                } //script section end
            } // steps section end
        } //stage upload to nexus section ends
//
//        echo 'DEV deployment is complete.'
//
        // TODO: Need DEV Approval button
        // TODO: Need OPS Approval button
        stage ('QA Ready for Deploy') {
            steps {
                timeout(time: 2, unit: 'DAYS') {
                    input message: 'Do you want to approve the deploy in QA?', ok: 'Yes', submitter: 'tn302af-admin-edit-view,tn036ad-view'
                }
            }
        }
//        echo 'QA deployment is started.'
//
//        stage('QA deployment') {
              // Checkout ci-cd QA Branch
              // oc-apply -f ${APPLICATION_NAME}.yaml
//        }
//        stage('QA verify') {
              // Hit health endpoint
//        }

//
//        echo 'QA deployment is complete.'
//
        // TODO: Need QA Approval
        // TODO: Need OPS Approval
        // TODO: Need UAT Ready for Deploy
        // TODO: Push docker image to NEXUS after QA

        //REPEAT ABOVE FOR ALL OTHER ENVIRONMENTS

//        echo 'UAT deployment is started.'
//        stage('UAT deployment') {
//            // Check the health endpoints
//            // Check the data dog results
//        }
//        stage ('UAT approval') {
//            timeout(time:2, unit:'DAYS') {
//                input message: 'Do you want to approve the deploy in PRODUCTION?', ok: 'Yes', submitter: 'tn302af-admin-edit-view,tn036ad-view'
//            }
//        }
//        PROD_APPROVAL = true
//        stage('PROD deployment') {
//            // Check the health endpoints
//            // Check the data dog results
//        }
    } //stages ends

} //pipeline ends


